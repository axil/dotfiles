# Source Prezto.
# https://github.com/sorin-ionescu/prezto

if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

HISTFILE=~/.zsh/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt autocd extendedglob nomatch notify
unsetopt beep

# https://gitlab.com/axil/dotfiles/-/blob/master/zsh/.zsh_functions
if [[ -f "$HOME/.zsh_functions" ]]; then
  source "$HOME/.zsh_functions"
fi

# For Arch Linux only
# https://wiki.archlinux.org/index.php/Pkgfile#Zsh
if [[ -f /usr/share/doc/pkgfile/command-not-found.zsh ]]; then
  source /usr/share/doc/pkgfile/command-not-found.zsh
fi

# tmuxinator shell completion
# https://github.com/tmuxinator/tmuxinator/blob/master/completion/tmuxinator.zsh
if [[ -f "$HOME/.tmuxinator/completion/tmuxinator.zsh" ]]; then
  source "$HOME/.tmuxinator/completion/tmuxinator.zsh"
fi

# Custom aliases
if [[ -f "$HOME/.zsh/aliases.plugin.zsh" ]]; then
  source "$HOME/.zsh/aliases.plugin.zsh"
fi

# Git aliases
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git
if [[ -f "$HOME/.zsh/git.plugin.zsh" ]]; then
  source "$HOME/.zsh/git.plugin.zsh"
fi

# Key bindings
# https://github.com/ohmyzsh/ohmyzsh/blob/master/lib/key-bindings.zsh
if [[ -f "$HOME/.zsh/key-bindings.zsh" ]]; then
  source "$HOME/.zsh/key-bindings.zsh"
fi

#
## PATH related stuff
#

PATH=$HOME/bin:$NPM_PACKAGES/bin:$GOPATH/bin:$PATH

## NPM packages in homedir
#NPM_PACKAGES="$HOME/.npm-packages"
## Unset manpath so we can inherit from /etc/manpath via the `manpath` command
#unset MANPATH  # delete if you already modified MANPATH elsewhere in your configuration
#MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
## Tell Node about these packages
#NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"

## GOPATH
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin

## Environmental variables
export TERMINAL=urxvt
export EDITOR=vim

## Ansible
export ANSIBLE_NOCOWS=1

# For asdf-vm
# Must be after PATH and after any ZSH framework
# https://asdf-vm.com/#/core-manage-asdf?id=add-to-your-shell
if [[ -f "$HOME/.asdf/asdf.sh" ]]; then
  source "$HOME/.asdf/asdf.sh"
  # append completions to fpath
  fpath=(${ASDF_DIR}/completions $fpath)
  # initialise completions with ZSH's compinit
  autoload -Uz compinit
  compinit
fi
