# Auto extension stuff
alias -s txt=vim
alias -s c=vim
alias -s cpp=vim
alias -s h=vim
alias -s errors=less
alias -s error=less
alias -s log=less
alias -s png=gqview
alias -s jpg=gqview
alias -s gif=gqview

# Normal stuff
alias -g G='| egrep --color=auto -n'
alias l='ls -G'
alias lll='ls++'
alias la='ls -alkh'
alias lk='ls -lkh'
alias name="uname -n"
alias gh='fc -l 0 | grep'                                # grep history
alias dh='dirs -v'                                       # directory history
alias ..='cd ..'
alias mem="free -m"
alias cd..='cd ..'
alias pa='ps aux'
alias pag='ps aux | grep -v grep | grep'
alias man='PAGER=less man -a'
alias cdc='cd ~; clear'
alias j='jobs'
alias h='history'
#alias grep='egrep'
alias ls='ls --color=auto'
alias ll='ls -lh --color=auto'
alias la='ls -a --color=auto'
alias lsa='ls -ld .*' # List only file beginning with "."
alias dt='dmesg | tail'
alias c='clear'
alias svim='sudo vim'
alias nt='sudo openvpn ~/.ntuavpn/ntua.ovpn'
alias font='fc-cache -f -v'
alias lock='sudo vlock -a'
alias kt='killall trayer'
alias mix='alsamixer'
alias sms='pysmssend&'
alias sex='startx'
alias p2='python2'
alias p3='python3'
alias ip2='ipython2'
alias ip3='ipython'
alias vi='vim'
alias vg='vim Gemfile'
alias play='ansible-playbook'

# configs
alias so='source ~/.zshrc'
alias za='vim ~/.zsh/aliases.plugin.zsh'
alias zf='vim ~/.zsh_functions'
alias z='vim ~/.zshrc'
alias paconf='svim /etc/pacman.conf'
alias sconf='vim $HOME/.ssh/config'
alias vd='vim Dockerfile'

# power
#alias off='dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop'
#alias boot='dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart'
#alias ram='dbus-send --system --print-reply --dest="org.freedesktop.UPower" /org/freedesktop/UPower org.freedesktop.UPower.Suspend'
#alias disk='dbus-send --system --print-reply --dest="org.freedesktop.UPower" /org/freedesktop/UPower org.freedesktop.UPower.Hibernate'

# network
alias wic='wicd-curses'
alias wid='sudo rc.d restart wicd'
alias net='restart network'
alias pn='ping -c2 ntua.gr'

# pacman
alias pacman='nice -n 19 pacman'
alias updatedb='sudo updatedb'
alias s='sudo pacman -S'
alias u='sudo pacman -U'
alias q='pacman -Q'
alias rncs='sudo pacman -Rncs'
alias se='pacman -Ss'
alias qs='pacman -Qs'
alias si='pacman -Si'
alias qi='pacman -Qi'
alias qo='pacman -Qo'
alias syy='sudo pacman -Syy'
alias suu='sudo pacman -Suu'
alias syyu='sudo pacman -Syyu --needed'
alias syu='yay'

# build
alias pkg='nice -n 19 makepkg --sign'
alias src='updpkgsums && makepkg --printsrcinfo >! .SRCINFO'
alias pkgi='makepkg -si'
alias build='sudo nice -n 19 extra-i686-build'
alias build64='sudo nice -n 19 extra-x86_64-build'
alias rpac='rm -rf pkg/ src/'
alias vp='vim PKGBUILD'
alias repgr86='pushd && cd $HOME/ArchGR/i686/ && rm -rf archlinuxgr.db.tar.gz && repo-add archlinuxgr.db.tar.gz *.tar.* && popd'
alias repgr64='pushd && cd $HOME/ArchGR/x86_64/ && rm -rf archlinuxgr.db.tar.gz && repo-add archlinuxgr.db.tar.gz *.tar.* && popd'
alias repgrany='pushd && cd $HOME/ArchGR/any/ && rm -rf archlinuxgr-any.db.tar.gz && repo-add archlinuxgr-any.db.tar.gz *.tar.* && popd'
alias repgr='repgr86 && repgr64 && repgrany'
alias rep='pushd && cd $HOME/aur/repo/ && repo-add --new --delta --verify custom.db.tar.gz *.pkg.tar.xz && popd'

# rvm
alias rl='rvm list'

## Tmux
alias muxdev='mux start gitlab-dev'
alias muxp='mux start personal'

alias mv='nocorrect mv'          # no spelling correction for mv
alias cp='nocorrect cp'          # no spelling correction for cp
alias mkdir='nocorrect mkdir'    # no spelling correction for mkdir

#alias lockscreen='xscreensaver-command -lock'           # xscreensaver-lock

# Global aliases -- These do not have to be
# at the beginning of the command line.
alias -g L='less'
alias -g M='more'
alias -g H='head'
alias -g T='tail'

alias vagrant='nice -n 19 vagrant'
alias android-connect="mtpfs -o allow_other /media/GalaxyNexus"
alias android-disconnect="fusermount -u /media/GalaxyNexus"
alias mac='sudo macchanger -m 00:24:be:77:18:82 enp0s25'
alias vr='vim ~/.vimrc'
alias myip='dig +short myip.opendns.com @resolver1.opendns.com'
alias vga='vim Vagrantfile'
alias pw='pwgen -sy 30 | head -n42'

alias gdel='git branch --merged master | grep -v "master" | xargs -n 1 git branch -d'
alias pull='git pull origin master && bundle && git checkout db/schema.rb && yarn'
alias migrate='bundle && rake db:migrate && git checkout db/schema.rb && yarn'
alias fixup='git commit --fixup $(git rev-parse HEAD) && git rebase -i @~2 --autosquash'
alias greb='git fetch origin master && git rebase origin/master'
alias shosts='sudo vim /etc/hosts'
alias dup='docker-compose up'
alias dupd='docker-compose up -d'
alias sre='sudo vim /etc/resolv.conf'
alias ipa='ip addr'
alias live='nanoc live -p 3004'
alias view='gwenview'
alias img='gwenview'
