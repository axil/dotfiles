dotfiles
========


## Vim

Plugins and themes to install:

```shell
git clone https://github.com/vim-airline/vim-airline ~/.vim/pack/dist/start/vim-airline
git clone https://github.com/edkolev/tmuxline.vim ~/.vim/pack/dist/start/tmuxline
```

- dracula
- lightline.vim
- tmuxline.vim - Follow https://powerline.readthedocs.io/en/master/installation/linux.html#fontconfig for the fonts
- vim-airline

